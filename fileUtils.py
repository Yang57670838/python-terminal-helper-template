import os
from tabnanny import check

current_dir = os.path.dirname(__file__)
file_path = os.path.join(current_dir, "mock/test.txt")

with open(file_path) as file:
    for line in file.readlines():
        print(line)

mynums = [1, 2, 3, 4, 5, 6]
print(list(filter(lambda num: num%2 == 0, mynums)))
print(list(filter(lambda num: num%2 == 0, mynums)))

