from colorama import init, Fore, Back

init(autoreset=True)

available_menu_options = ['Generate Template', 'Exit']

# menu
def print_menu():
    print('Welcome, you have the following options:')
    for index, option in enumerate(available_menu_options):
        print(Fore.GREEN + f'{index + 1}. {option}')
    print('\n')


# check options
def ask_valid_options():
    print_menu()
    while True:
        selected_option = input('Please select which option do you want to continue..\n')
        if selected_option.isdigit():
            if int(selected_option) in range(1, 3):
                return int(selected_option)
            else:
                print(Fore.RED + 'please select valid options..')
        else:
            print(Fore.RED + 'please select valid number options..')


# main
selectedOption = ask_valid_options()
if selectedOption == 1:
    print(Fore.GREEN + "Start building template...")
    # TODO...
elif selectedOption == 2:
    print('bye bye..')






def template_choice():
    available_templates = ['react', 'nestjs', 'terraform']
    choice = 'wrong'

    while choice not in available_templates:
        choice = input('which project you want to setup?\n')

        if choice not in available_templates:
            print(Fore.RED + 'please select available templates:')
            print(Fore.YELLOW + 'react')
            print(Fore.YELLOW + 'nestjs')
            print(Fore.YELLOW + 'terraform')

    return choice


def is_keep_generate_template():
    choice = 'invalid'

    while choice not in ['Y', 'N']:
        choice = input('Do you want to keep generate a new template (Y or N)\n')
        if choice not in ['Y', 'N']:
            print('Sorry I dont understand, please choose Y or N')
        
    if choice == 'Y':
        return True
    else:
        return False

keep_generate = True

while keep_generate:
    temnplate = template_choice()
    print(Fore.GREEN + "Start building template for " + temnplate + "...")
    keep_generate = is_keep_generate_template()

    

#Set autoreset to True else style configurations would be forwarded to the next print statement
print(Fore.YELLOW + "Are you convinced yet?")
print(Fore.GREEN + "Once you go colorama, you never go back-around(a)") #Find better joke
print('test')
print(Fore.YELLOW + Back.RED + "A WILD WARNING HAS APPEARED")
print(Fore.RED + "found template in db")

