# compare two lists are same
suits1 = ["😈", "😵", "🤢", "😨", "😃"]
suits2 = ["😃", "😈", "😵", "🤢", "😨"]

suits1.sort()
suits2.sort()

print(suits1)
print(suits2)
if suits1 == suits2:
    print("The lists are identical")
else:
    print("The lists are not identical")



# compare all elements in a list are same
def all_same(items):
    return all(x == items[0] for x in items)

property_list = ["one", "one", "one"]
print(all_same(property_list))

property_list = ["one", "one", "two"]
print(all_same(property_list))

print(all_same([]))

# one lists is subset of another or not..
list_picked = ["😃", "😈", "😵", "🤢", "😨", "2311", "dsad"]
list2 = ["😃", "😈", "😵", "🤢", "😨"]

if (all(item in list_picked for item in list2)):
    print('correct')