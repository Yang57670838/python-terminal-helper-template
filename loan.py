# simply calculate after how many months, how is the loan status
money_owed = float(input('How mych money do you owe, in dollars? \n')) #50000
rate = float(input('What is the annual percentage rate of the load? \n')) # 3 for 3% for example
monthly_payment = float(input('What is your current monthly payment? \n')) # 2000 for example
months = int(float(input('How many months do you want to calculate for? \n'))) # 24 months for example

# because the rate is yearly rate, we pay interest month, so we need to calculate monthly rate
monthly_rate = rate/100/12

for i in range(months):
    interest_paid = money_owed * monthly_rate
    # this month owed money need to add interest need to pay for this month
    money_owed = money_owed + interest_paid

    if (money_owed - monthly_payment < 0):
        print('The last payment is', money_owed)
        print('You paid off the loan in', i+1, 'month')
        break

    # make payment for this month by minus the monthly_payment
    money_owed = money_owed - monthly_payment

print('Now I owe', money_owed, 'after', months, 'months')


