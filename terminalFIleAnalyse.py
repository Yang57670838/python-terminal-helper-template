from colorama import init, Fore, Back

init(autoreset = True)

file_list = []

def file_num_choice():
    num_of_files_for_scan = 'INVALID'
    input_within_range = False

    while num_of_files_for_scan.isdigit() == False or input_within_range == False:
        num_of_files_for_scan = input(Fore.GREEN + 'How many files do you want to scan together (1-5)\n')
        # digit check
        if num_of_files_for_scan.isdigit() == False:
            print(Fore.RED + "Please enter a valid number")
        # range check
        if num_of_files_for_scan.isdigit():
            if int(num_of_files_for_scan) in range(1, 5):
                input_within_range = True
            else:
                print(Fore.RED + "We can only scan 1-5 files together")
    return int(num_of_files_for_scan)

num_files = file_num_choice()

for i in range(num_files):
    file_list.append(input('Enter the file name with path:'))

print(file_list)





#Set autoreset to True else style configurations would be forwarded to the next print statement
print(Fore.RED + "Hello World")
print(Fore.YELLOW + "Are you convinced yet?")
print(Fore.GREEN + "Once you go colorama, you never go back-around(a)") #Find better joke
print('test')
print(Fore.YELLOW + Back.RED + "A WILD WARNING HAS APPEARED")

