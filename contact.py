mock_profile = {
    'name': 'David Jones',
    'relationship_contacts': [
        {
            'name': 'Tom', 'relationship': 'friend', 'email': 'tom@gmail.com'
        },
        {
            'name': 'Jerry', 'relationship': 'friend', 'email': 'jerry@gmail.com'
        },
        {
            'name': 'Jess', 'relationship': 'partner', 'email': 'jess@gmail.com'
        }
    ]
}

for contact in mock_profile['relationship_contacts']:
    print(contact['email'])