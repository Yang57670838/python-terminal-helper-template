'''
Yang Liu
34697705
created_at: 2024/04/06
updated_at: 2024/04/07
description: This program
'''

from random import shuffle, randint, choice

# init global variables
suits1 = ["♥", "♦", "♣", "♠"]
suits2 = ["😃", "😈", "😵", "🤢", "😨"]
suits3 = ["🤡", "👹", "👺", "👻", "👽", "👾", "🤖"]
available_suit_types = [suits1, suits2, suits3]
selected_suit_type = []

available_menu_options = ['Start Game', 'Pick a Card',
                          'Shuffle Deck', 'Show My Cards', 'Check Win/Lose', 'Exit']

values = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
values_mapper = {
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    '10': 10,
    'J': 11,
    'Q': 12,
    'K': 13,
    'A': 1
}

deck = []

player_cards = []
robot_cards = []

# Game menu function
def print_game_menu():
    print('Welcome to card game, you have the following options:')
    # print menu options by a loop
    for index, option in enumerate(available_menu_options):
        print(f'{index + 1}. {option}')
    print('\n')
    print('You have following three types of suits can choose as second optional input, or suit 1 will be the default suit type')
    print('suit type 1: ' + ' '.join(suits1))
    print('suit type 2: ' + ' '.join(suits2))
    print('suit type 3: ' + ' '.join(suits3))
    print('\n\n')


# Create Deck function
def generate_deck(selected_suits):
    # update global variable since no return value for this function
    global deck
    deck = [f"{suits} {values}" for values in values for suits in selected_suits]
    # update global selected_suit_type so later can be used anywhere
    global selected_suit_type
    selected_suit_type = selected_suits


# Shuffle Deck function while freezing leading, trailing, and middle cards
# pass global variable deck as argument of this function, update the argument inside the function will also update outside global variable directly, since Python treats it as reference
def shuffle_deck(deck_for_shuffle, suits):
    leading_card = f"{suits[0]} A"
    middle_card = f"{suits[1]} Q"
    traling_card = f"{suits[-1]} K"
    middle_position_index = round((len(deck_for_shuffle) - 1)/2)
    # shuffle
    shuffle(deck_for_shuffle)
    # its possible that its first time shuffle after create deck, so will re-position everytime for leading and trailing cards
    deck_for_shuffle.remove(leading_card)
    deck_for_shuffle.insert(0, leading_card)
    deck_for_shuffle.remove(traling_card)
    deck_for_shuffle.append(traling_card)
    deck_for_shuffle.remove(middle_card)
    deck_for_shuffle.insert(middle_position_index, middle_card)
    print('Shuffled deck is: ' + ' '.join(deck_for_shuffle))


# Pick Card function, leading, trailing, and middle cards which are freezing and can not be picked
# since when pick the freezing card, it will be ignored and allow to re-pick, so will make this an recursion function until picked a not freezing card
# again, pass deck argument as global variable reference, so update it directly inside function will affect global variable outside
def pick_a_card(deck_for_pick):
    leading_card_index = 0
    middle_card_index = round((len(deck_for_pick) - 1)/2)
    traling_card_index = len(deck_for_pick) - 1
    # randomly select a card's index
    picked_index = randint(leading_card_index, traling_card_index)
    if picked_index == leading_card_index or picked_index == middle_card_index or picked_index == traling_card_index:
        return pick_a_card(deck_for_pick)
    else:
        # assign the picked card value to a new variable, before its get removed from list
        result = deck_for_pick[picked_index]
        # remove picked card from global deck list by update the global variable reference
        del deck_for_pick[picked_index]
        return result


# Show Cards function
def show_player_cards(player_cards):
    print('Currently, you are holding cards: ' + ' '.join(player_cards))


# check Rule 1 is matching or not, return 'player' or 'robot' or None
def is_winner_from_rule_1(player_cards, robot_cards, suits):
    # check player's cards
    player_values = []
    for card in player_cards:
        elements = card.split()
        player_values.append(elements[1])
    # since every cards value/suit are unique combination in one deck, all the same value cards must have different suits
    # if number of them are same as number of available suits, means they satisfy rule 1
    # so first get count of most frequently same value from all of the player cards
    player_common_card_counter = 0
    for i in player_values:
        curr_counter = player_values.count(i)
        if (curr_counter > player_common_card_counter):
            player_common_card_counter = curr_counter
    # if number of common value cards are same as number of available suits, means they satisfy rule 1
    if player_common_card_counter == len(suits):
        return 'player'
    # check robot's cards
    robot_values = []
    for card in robot_cards:
        elements = card.split()
        robot_values.append(elements[1])
    # since every cards value/suit are unique combination in one deck, all the same value cards must have different suits
    # if number of them are same as number of available suits, means they satisfy rule 1
    # so first get count of most frequently same value from all of the robot cards
    robot_common_card_counter = 0
    for i in robot_values:
        curr_counter = robot_values.count(i)
        if (curr_counter > robot_common_card_counter):
            robot_common_card_counter = curr_counter
    # if number of common value cards are same as number of available suits, means they satisfy rule 1
    if robot_common_card_counter == len(suits):
        return 'robot'
    # else, means rule 1 not match for both player and robot
    return None


# check Rule 2 is matching or not, return 'player' or 'robot' or None
def is_winner_from_rule_2(player_cards, robot_cards, suits):
    # check player's cards
    player_values = []
    for card in player_cards:
        elements = card.split()
        player_values.append(elements[1])
    # get count of most frequently same value from all of the player cards
    player_common_card_counter = 0
    for i in player_values:
        curr_counter = player_values.count(i)
        if (curr_counter > player_common_card_counter):
            player_common_card_counter = curr_counter
    # if number of common value cards are same as number of available suits minus 1, means they satisfy rule 2
    if player_common_card_counter == len(suits) - 1:
        return 'player'
    # check robot's cards
    robot_values = []
    for card in robot_cards:
        elements = card.split()
        robot_values.append(elements[1])
    # get count of most frequently same value from all of the robot cards
    robot_common_card_counter = 0
    for i in robot_values:
        curr_counter = robot_values.count(i)
        if (curr_counter > robot_common_card_counter):
            robot_common_card_counter = curr_counter
    # if number of common value cards are same as number of available suits minus 1, means they satisfy rule 2
    if robot_common_card_counter == len(suits) - 1:
        return 'robot'
    # else, means rule 2 not match for both player and robot
    return None


# check Rule 3 is matching or not, return 'player' or 'robot' or None
def is_winner_from_rule_3(player_cards, robot_cards, suits):
    second_suit = suits[1]
    # count for player
    player_suits = []
    for card in player_cards:
        elements = card.split()
        player_suits.append(elements[0])
    player_count = player_suits.count(second_suit)
    # count for player
    robot_suits = []
    for card in robot_cards:
        elements = card.split()
        robot_suits.append(elements[0])
    robot_count = robot_suits.count(second_suit)
    # compare
    if player_count > robot_count:
        return 'player'
    elif player_count < robot_count:
        return 'robot'
    else:
        return None


# check Rule 4 is matching or not, return 'player' or 'robot' or None
def is_winner_from_rule_4(player_cards, robot_cards):
    # calculate average for player
    player_average = 0
    if len(player_cards) > 0:
        player_values_total = 0
        for card in player_cards:
            elements = card.split()
            # convert to int value by defined mapped in the beginning
            value = values_mapper.get(elements[1]) or 0
            player_values_total += value
        player_average = player_values_total / len(player_cards)
    # calculate average for robot
    robot_average = 0
    if len(robot_cards) > 0:
        robot_values_total = 0
        for card in robot_cards:
            elements = card.split()
            # convert to int value by defined mapped in the beginning
            value = values_mapper.get(elements[1]) or 0
            robot_values_total += value
        robot_average = robot_values_total / len(robot_cards)
    # compare
    if player_average > robot_average:
        return 'player'
    elif player_average < robot_average:
        return 'robot'
    else:
        return None


# Check Result function, return True or False to indicate whether the player has won or lost
def check_result(player_cards, robot_cards, suits):
    if len(player_cards) > 0 and len(robot_cards) == 0:
        return True
    if len(player_cards) == 0 and len(robot_cards) == 0:
        return False
    # rule 1 check
    rule1_result = is_winner_from_rule_1(player_cards, robot_cards, suits)
    if rule1_result == 'player':
        return True
    elif rule1_result == 'robot':
        return False
    # rule 2 check
    rule2_result = is_winner_from_rule_2(player_cards, robot_cards, suits)
    if rule2_result == 'player':
        return True
    elif rule2_result == 'robot':
        return False
    # rule 3 check
    rule3_result = is_winner_from_rule_3(player_cards, robot_cards, suits)
    if rule3_result == 'player':
        return True
    elif rule3_result == 'robot':
        return False
    # rule 4 check
    rule4_result = is_winner_from_rule_4(player_cards, robot_cards)
    if rule4_result == 'player':
        return True
    elif rule4_result == 'robot':
        return False
    return False


# Play Game function
# first create a function to catch valid user input, then start the game by another function
def render_menu_with_input():
    # init variables
    selected_game_option = -1
    selected_suits_option = -1

    # print menu
    print_game_menu()

    is_invalid_input = True
    while is_invalid_input:
        # ask user input
        user_input = input('Please enter your selection: ')
        # trim leading and trailing white space first
        value = user_input.strip()
        # split white space if there are more inputs
        choices = value.split()
        # validate input
        if len(choices) == 0 or len(choices) > 2:
            print('Only one or two inputs are allowed')
        if len(choices) == 1:
            if choices[0].isdigit():
                if int(choices[0]) in range(1, 7):
                    is_invalid_input = False
                    selected_game_option = int(choices[0])
                    # assume default suit type is the first option from the suits list
                    selected_suits_option = 1
                else:
                    print('There are only 6 options available in the game menu')
            else:
                print('Only digit inputs are allowed')
        if len(choices) == 2:
            if choices[0].isdigit() and choices[1].isdigit():
                if int(choices[0]) in range(1, 7) and int(choices[1]) in range(1, 4):
                    is_invalid_input = False
                    selected_game_option = int(choices[0])
                    selected_suits_option = int(choices[1])
                else:
                    print(
                        'There are only 6 options available in the game menu, and 3 available suit types')
            else:
                print('Only digit inputs are allowed')
    print(
        f'You have selected {available_menu_options[selected_game_option - 1]}')
    print('You have selected suits with: ' +
          ' '.join(available_suit_types[selected_suits_option - 1]) + '\n')
    return [selected_game_option, selected_suits_option]


# start game
def start_game():
    is_game_continue = True
    global player_cards
    global robot_cards
    while is_game_continue:
        success_input_result = render_menu_with_input()
        if success_input_result[0] == 1:
            # generate new deck if Start Game
            generate_deck(available_suit_types[success_input_result[1] - 1])
            # shuffle deck
            shuffle_deck(
                deck, available_suit_types[success_input_result[1] - 1])
            print('game started....... continue to pick card or view cards')
        elif success_input_result[0] == 2:
            if len(deck) == 0:
                print(
                    'You have to start the game first before you can pick a card..........')
            else:
                # pick one card for player
                player_picked_card = pick_a_card(deck)
                player_cards.append(player_picked_card)
                print('You have picked card........... ' + player_picked_card)
                # The program could pick no card for the robot
                # instead of add no card option into the pick_a_card function, here randomly choose if robot will pick card or not first
                random_no_pick_for_robot = choice(['yes', 'no'])
                if random_no_pick_for_robot == 'yes':
                    print('Robot picks no card this time...........')
                else:
                    # pick one card for robot
                    robot_picked_card = pick_a_card(deck)
                    robot_cards.append(robot_picked_card)
                    print('Robot has picked card........... ' + robot_picked_card)
                # check if player has picked 6 maximum cards or not
                if len(player_cards) == 6:
                    # check final result
                    result = check_result(
                        player_cards, robot_cards, selected_suit_type)
                    if result:
                        print('Yes!!!!! You won!!!!!!!!!!!!!!!!!')
                    else:
                        print(
                            'Robot has won this game... but dont worry, lets try again')
                    print('Game will restart now..............')
                    # clean up holding cards
                    player_cards = []
                    robot_cards = []
                    # generate new deck with original picked suits
                    generate_deck(selected_suit_type)
                     # shuffle deck
                    shuffle_deck(deck, selected_suit_type)
                    print('game started....... continue to pick card or view cards')
        elif success_input_result[0] == 3:
            if len(deck) == 0:
                print(
                    'You have to start the game first before you can shuffle the deck..........')
            else:
                # shuffle deck
                shuffle_deck(deck, selected_suit_type)
        elif success_input_result[0] == 4:
            print('\n\n')
            # show player cards
            show_player_cards(player_cards)
        elif success_input_result[0] == 5:
            print('\n\n')
            result = check_result(
                player_cards, robot_cards, selected_suit_type)
            if result:
                print('You are winning currently!!!!')
            else:
                print('Robot is winning... but dont worry, keep trying!')
        elif success_input_result[0] == 6:
            print('Bye bye..')
            is_game_continue = False
        else:
            print('\n')
            print('Something went wrong! Please reselect the game option.')


start_game()
